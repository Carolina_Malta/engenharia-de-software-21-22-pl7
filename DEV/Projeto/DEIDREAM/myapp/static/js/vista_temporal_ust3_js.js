let pontos = document.querySelectorAll(".ponto")
let cartoes = document.querySelectorAll(".cartao")  // geral corresponde ao conteúdo dos commits
let cartoes_grandes = document.querySelectorAll(".cartao_grande")

for (let i = 0; i < pontos.length; i++) {
    // à partida mapea-se as datas mete-se num array
    pontos[i].style.top = 24 + "px" + i * 36 + "px";
}

for (let i = 0; i < pontos.length; i++) {
    for (let c = 0; c < cartoes.length; c++) {// supostamente revela só o cartao correspondente ào ponto clicado!!!!
        //   for (let cg = 0; cg < cartoes_grandes.length ; cg++) {
        pontos[i].addEventListener("click", function () {
            //  cartoes[c].style.top= i * 18 + "px";
            cartoes_grandes[c].classList.add("desaparece");
            if (c === i) {
                cartoes[c].classList.toggle("desaparece");
            }

            else {
                cartoes[c].classList.add("desaparece"); // para se clicar num outro cartao, o 1º desaparecer
            }

            if (c % 2 == 1)             // para os cartões ficarem em zigue-zague
                cartoes[c].style = "left: 53%;"
        })
    }
}


let setas_cartao_p = document.querySelectorAll(".setas");
let setas_cartao_grande = document.querySelectorAll(".setas_grandes")

for (let s = 0; s < setas_cartao_p.length; s++) {
    setas_cartao_p[s].addEventListener("click", function () {
        for (let c = 0; c < cartoes.length; c++) {
            if (c === s) {
                cartoes_grandes[c].classList.toggle("desaparece");
                cartoes_grandes[c].classList.toggle("aparece");

                cartoes_grandes[c].style = "z-index: 2;"
            }

            else {
                cartoes_grandes[c].classList.add("desaparece");
            }
        }
    })
}
for (let sg = 0; sg < setas_cartao_grande.length; sg++) {
    setas_cartao_grande[sg].addEventListener("click", function () {
        for (let cg = 0; cg < cartoes_grandes.length; cg++) {
            if (cg === sg)
                cartoes_grandes[cg].classList.toggle("desaparece");
            else
                cartoes_grandes[cg].classList.add("desaparece");

        }
    })
}

for (let p = 0; p < pontos.length; p++) {
    for (let cg = 0; cg < cartoes_grandes.length; cg++) {
        if (cg != p && cartoes_grandes[cg].classList.contains("aparece")) {
            cartoes_grandes[cg].classList.style.display = "none";
        }

    }
}