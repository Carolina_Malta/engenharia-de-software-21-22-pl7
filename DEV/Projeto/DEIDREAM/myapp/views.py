"""DEIDREAM VIEWS Configuration

Each view class can return the desired info
and will render the associated html page
"""
from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth.hashers import make_password

from .forms import LoginForm,SignUpForm

from json import dumps
from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login, logout

from .forms import DateForm                 # import forms

import requests                             # requests library to do api calls

from datetime import datetime               # datetime library


# Functions

def days_between(d1, d2):
    d1 = datetime.strptime(d1, "%Y-%m-%d")
    d2 = datetime.strptime(d2, "%Y-%m-%d")
    return (d2 - d1).days



# Create your views here.


# primeira view
class primeira_view(View):
    def get(self, request):
        return redirect('login')

    def post(self, request):
        pass


# login view
class login_view(View):
    def get(self, request):

        login_form = LoginForm()

        return render(request, 'login.html', {'login_form': login_form})

    def post(self, request):
        login_form = LoginForm(request.POST)

        if login_form.is_valid():

            username = login_form.cleaned_data.get('username')
            password = login_form.cleaned_data.get('password')

            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)

                return redirect('commits')
            else:
                return redirect('login')
        else:
            return redirect('login')

class sign_up_view(View):
    def get(self,request):
        print("Entrei no GET")

        signup_form=SignUpForm()

        return render (request,'signup.html',{'signup_form':signup_form})

    def post(self,request):
        print("Nada, por agora.")

        signup_form=SignUpForm(request.POST)

        if signup_form.is_valid():
            signup_form=signup_form.save(commit=False)

            username=signup_form.username
            password=signup_form.password
            signup_form.password= make_password(password)

            signup_form.save()

            user=authenticate(request,username=username,password=password)

            if user is not None:
                login(request,user)

                return redirect('commits')
            return redirect('signup')
        return redirect('signup')

def logout_view(request):
    logout(request)
    return redirect('login')

# commits view
class commits_view(View):

    # get commits info from gitlab
    def get(self, request):
        if request.user.is_authenticated:
            date_form = DateForm()

            #params = { "access_token": "" }                                                         # parameters given to our url
            
            url = 'https://gitlab.com/api/v4/projects/29898682/repository/commits?page='            # url to access each commits page
            data = [{}]                                                                             # data we get from each page
            page_num = 1                                                                            # page number iterator

            commits_list =  []                                                                      # list to store all commits
            aux_dict = {}                                                                           # auxiliar dictionary to store individual commits
            
            while len(data) != 0:                                                                   # check each page until nothing is returned
                aux_url = url + str(page_num)                                                       # auxiliary url that iterates through each page

                response = requests.get(aux_url)                                     # response from our url (gitlab api)
                data = response.json()                                                              # convert our response to something readable

                if type(data) is not list:                                                          # something went wrong, print data content and break the loop
                    print(data)
                    print('\n::Warning:: The problem might be your \"access_token\"\n')
                    break

                for commit in data:                                                                 # fill auxiliar dictionary
                    aux_dict['author_name'] = commit['author_name']
                    aux_dict['title'] = commit['title']
                    aux_dict['committed_date'] = commit['committed_date'][0:10]
                    aux_dict['committed_date'] += (' ' + commit['committed_date'][11:19])

                    commits_list.append(aux_dict)                                                   # add auxiliar dictionary to list
                    aux_dict = {}                                                                   # reset auxiliar dictionary

                page_num += 1                                                                       # next page

            if (len(commits_list) != 0):                                                            # get first commit date
                first_date = (commits_list[-1])['committed_date'][0:10]

            for aux_dict in commits_list:                                                           # add index to commits dictionary list
                aux_dict['index'] = days_between(first_date, aux_dict['committed_date'][0:10])

            return render(request, 'commits.html', {'date_form': date_form, 'commits_list': reversed(commits_list)})          # return our page and the desired commits list
        
        return redirect('login')
    
    def post(self, request):

        date_form = DateForm(request.POST)                                                      # use django html forms to get dates

        date1 = None                                                                            # start date
        date2 = None                                                                            # end date

        if date_form.is_valid():                                                                # check if the form is correctly filled
            date1 = date_form.cleaned_data.get('date1')
            date2 = date_form.cleaned_data.get('date2')

            if(date1 > date2):                                                                  # sort dates
                date3 = date1
                date1 = date2
                date2 = date3

        #params = { "access_token": "" }                                                         # parameters given to our url
        
        url = 'https://gitlab.com/api/v4/projects/29898682/repository/commits?page='            # url to access each commits page
        data = [{}]                                                                             # data we get from each page
        page_num = 1                                                                            # page number iterator

        commits_list =  []                                                                      # list to store all commits
        aux_dict = {}                                                                           # auxiliar dictionary to store individual commits
        
        while len(data) != 0:                                                                   # check each page until nothing is returned
            aux_url = url + str(page_num)                                                       # auxiliary url that iterates through each page

            response = requests.get(aux_url)                                     # response from our url (gitlab api)
            data = response.json()                                                              # convert our response to something readable

            if type(data) is not list:                                                          # something went wrong, print data content and break the loop
                print(data)
                print('\n::Warning:: The problem might be your \"access_token\"\n')
                break

            for commit in data:                                                                 # fill auxiliar dictionary

                if (date_form.is_valid()):                                                      # form is correctly filled, print commits in interval
                
                    if (commit['committed_date'][0:10] >= str(date1) and commit['committed_date'][0:10] <= str(date2)):
                        aux_dict['author_name'] = commit['author_name']
                        aux_dict['title'] = commit['title']
                        aux_dict['committed_date'] = commit['committed_date'][0:10]
                        aux_dict['committed_date'] += (' ' + commit['committed_date'][11:19])

                        commits_list.append(aux_dict)                                           # add auxiliar dictionary to list
                        aux_dict = {}                                                           # reset auxiliar dictionary

                elif (not date_form.is_valid()):                                                # form is not correctly filled, print all commits
                    aux_dict['author_name'] = commit['author_name']
                    aux_dict['title'] = commit['title']
                    aux_dict['committed_date'] = commit['committed_date'][0:10]
                    aux_dict['committed_date'] += (' ' + commit['committed_date'][11:19])

                    commits_list.append(aux_dict)                                               # add auxiliar dictionary to list
                    aux_dict = {}                                                               # reset auxiliar dictionary

            page_num += 1                                                                       # next page

        if (len(commits_list) != 0):                                                            # get first commit date
            first_date = (commits_list[-1])['committed_date'][0:10]

        for aux_dict in commits_list:                                                           # add index to commits dictionary list
            aux_dict['index'] = days_between(first_date, aux_dict['committed_date'][0:10])

        return render(request, 'commits.html', {'date_form': date_form, 'commits_list': reversed(commits_list), 'date1': date1, 'date2': date2})


# members view
"""
def getTotalPages(url, params = {'access_token': ''}):                                         
    return int((requests.head(url, params = params).headers)['X-Total-Pages'])
"""

class members_view(View):
    """
    # get members info from gitlab
    def get(self, request):
        url = 'https://gitlab.com/api/v4/projects/29898682/members/all?per_page=100&page='
        params = {"access_token": ""}
        totalPages = getTotalPages(url,params)+1
        members = {}

        for page in range(1, totalPages):
            urlAux = url + str(page)
            data = (requests.get(urlAux, params = params)).json()
            for member in data:
                members[member['name']] = {'hours': 0, 'avatar_url': member['avatar_url']}
        
        url = 'https://gitlab.com/api/v4/projects/29898682/issues?per_pag=100&page='
        totalPages = getTotalPages(url,params)+1

        for page in range(1,totalPages):
            urlAux = url + str(page)
            data = (requests.get(urlAux, params = params)).json()
            for issue in data:
                assignee = issue['assignee']
                
                if (assignee == None):
                    continue

                if(type(assignee) != list):
                    assigneeList = [assignee]
                else:
                    assigneeList = assignee

                for person in assigneeList:
                    (members[person['name']])['hours'] += (issue['time_stats'])['total_time_spent']
        

        return render(request, 'members.html', {'members' : members})
        """

    # get members info from gitlab
    def get(self, request):

        
        members = {}                                                                            # members dictionary
        if request.user.is_authenticated :

            # fill members dictionary with sub dictionaries that contain members info
            url = 'https://gitlab.com/api/v4/projects/29898682/users?page='                         # url to access each users page
            data = [{}]                                                                             # data we get from each page
            page_num = 1                                                                            # page number iterator

            while len(data) != 0:                                                                   # check each page until nothing is returned
                aux_url = url + str(page_num)                                                       # auxiliary url that iterates through each page

                response = requests.get(aux_url)                                     # response from our url (gitlab api)
                data = response.json()                                                              # convert our response to something readable

                if type(data) is not list:                                                          # something went wrong, print data content and break the loop
                    print(data)
                    print('\n::Warning:: The problem might be your \"access_token\"\n')
                    break

                for member in data:
                    members[member['name']] = {'hours': 0, 'avatar_url': member['avatar_url']}

                page_num += 1                                                                       # next page



            # fill members sub dictionaries with hours spent in issues
            url = 'https://gitlab.com/api/v4/projects/29898682/issues?page='                        # url to access each issues page
            data = [{}]                                                                             # reset data
            page_num = 1                                                                            # reset page iterator

            while len(data) != 0:                                                                   # check each page until nothing is returned
                aux_url = url + str(page_num)                                                       # auxiliary url that iterates through each page

                response = requests.get(aux_url)                                     # response from our url (gitlab api)
                data = response.json()                                                              # convert our response to something readable

                if type(data) is not list:                                                          # something went wrong, print data content and break the loop
                    print(data)
                    print('\n::Warning:: The problem might be your \"access_token\"\n')
                    break

                for issue in data:
                    assignee = issue['assignee']
                    
                    if (assignee == None):
                        continue

                    if(type(assignee) != list):
                        assigneeList = [assignee]
                    else:
                        assigneeList = assignee

                    for person in assigneeList:
                        (members[person['name']])['hours'] += (issue['time_stats'])['total_time_spent']

                page_num += 1                                                                       # next page

            active_members = {}

            for (member, value)  in members.items():
                if value['hours'] > 0:
                    active_members[member] = value

            return render(request, 'members.html', {'members' : active_members})                           # return our page and the desired members list
        return redirect('login')
        
    def post(self, request):
        pass
