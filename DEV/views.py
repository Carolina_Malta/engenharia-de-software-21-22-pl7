"""DEIDREAM VIEWS Configuration

Each view class can return the desired info
and will render the associated html page
"""

# Imports and global vars

from django.shortcuts import render
from django.views.generic import View

import requests                             # requests library to do api calls

debug = False                               # debug variable



# Create your views here.

# primeira view
class primeira_view(View):

    def get(self, request):
        print("Entrei no get")
        return render(request, 'pagina.html')
    
    def post(self, request):
        print("Nada")

# commits view
class commits_view(View):

    # get commits info from gitlab
    def get(self, request):
        params = { "access_token": "" }                       # parameters given to our url
        
        url = 'https://gitlab.com/api/v4/projects/29898682/repository/commits?page='    # url to access each commits page
        data = [{}]                                                                     # data we get from each page
        page_num = 1                                                                    # page number iterator

        commits_list =  []                                                              # list to store all commits
        aux_dict = {}                                                                   # auxiliar dictionary to store individual commits
        
        while len(data) != 0:                                                           # check each page until nothing is returned
            aux_url = url + str(page_num)                                               # auxiliary url that iterates through each page

            response = requests.get(aux_url, params=params)                             # response from our url (gitlab api)
            data = response.json()                                                      # convert our response to something readable

            if type(data) is not list:                                                  # something went wrong, print data content and break the loop
                print(data)
                print('\n::Warning:: The problem might be your \"access_token\"\n')
                break

            for commit in data:                                                         # fill auxiliar dictionary
                aux_dict['author_name'] = commit['author_name']
                aux_dict['title'] = commit['title']
                aux_dict['committed_date'] = commit['committed_date']

                commits_list.append(aux_dict)                                           # add auxiliar dictionary to list
                aux_dict = {}                                                           # reset auxiliar dictionary

            page_num += 1                                                               # next page

        if debug:                                                                       # debugging
            for commit in commits_list:
                print(commit['author_name'])
                print(commit['title'])
                print(commit['committed_date'])
                print('')
            print('Total Commits:', len(commits_list))

        return render(request, 'commits.html', {'commits_list': commits_list})          # return our page and the desired commits list

    def post(self, request):
        return render(request, 'commits.html')