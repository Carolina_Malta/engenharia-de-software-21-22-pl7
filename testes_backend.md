|INSTRUÇÃO|OBJETIVO|Estado de verificaçao|REQUISITO|
|--|--|--|--|--|
|Entrar no link da dashboard.Escrever os dados no local de login|Verificar se escreve||38|
|Estando no dashboard, tentar fazer login com credenciais erradas, username e/ou password erradas|Verificar se não deixa aceder ao url correto||38|
||Verificar se pede os dados de novo||38|
|Estando no dashboard, introduzir credenciais todas em CAPS.|Verificar CAPS-sensibility da programação||38|
|Estando no dashboard, introduzir credenciais todas em minúsculas|Verificar sensibilidade a minúsculas||38|
|No dashboard, escrever fora dos locais definidos|Verificar impossibilidade de escrita fora dos locais convencionados||38|
|Introduzir as credenciais corretas no dashboard.|Verificar redireção automática para url correto||38|
|Ao dar login, ver a timeline de commits|Ser possivel ver a timeline||3|
|Dando login corretamente, ver a timeline de commits|Verificar se é inteligível quando foi efetuado o commit e o intervalo de tempo entre ele e o anterior||3|
|Dando login correto, ver a timeline de commits|Verificar se há graduação que facilite situar o commit em unidades temporais||3a|
|Dentro da timeline de commits, ter opçao de procura entre datas|Poder verificar corretamente commits entre as datas especificas||3|
|Dentro da timeline de commits, colocar uma datas de procura ao contrario, data mais tarde primeiro e mais cedo depois|Gerar erro, ou nao ser possivel|||3|
|Efetuar um commit|Verificar mudança de estado da timeline ao dar refresh da pagina.||3/3a|
|Estando na dashboard, ter a opçao de ver a página de membros ativos|Haver a opçao para ver a pagina||19|
|No dashboard, pressionar o botao para ver os membros ativos|Verificar que se listou os membros||19|
