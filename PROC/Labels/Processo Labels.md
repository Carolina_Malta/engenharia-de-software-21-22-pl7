## Descrição das labels

**Arch:**
- Ficheiros relacionados com arquitetura e tecnologias;

**DES:**
- Design de software detalhado (a estrutura e não a parte visual);

**DEV:**
- Código e ficheiros relacionados;

**ENV:**
- Configurações IDE;
- Configurações CI/CD;

**PM:**
- Ficheiros de gerenciamento do projeto;

**PROC:**
- Lista com todos os processos/regras;

**PROD:**
- Ficheiros de produto/entrega (com versão);

**QA:**
- Ficheiros de garantia de qualidade dos processos;

**REQ:**
- Ficheiros relacionados com requisitos do cliente, do produto e técnicos;

**ata:**
- Issue relacionado à ata;

**Prioridade Máxima:**
- Tarefas com prioridade máxima no desenvolvimento do projeto. Estas são as que devem percorrer em primeiro lugar todos os momentos até o deploy (Design, visual e arquitetura, Implementação e Testes);

**Em execução:**
- Issue está a ser executada;

**Por Rever:**
- Issue necessita de ser revista pelo seu criador ou por algum membro da equipa de qualidade;

**Revisto:**
- Issue foi revista e deverá ter permissão para fechar.