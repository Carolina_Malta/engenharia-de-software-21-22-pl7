# Arquitetura C4

## Razões da escolha da Arquitetura C4

Foi escolhida este modelo devido à facilidade de apresentar todos os aspetos do projeto. O modelo C4 está normalmente dividido, tal como o nome indica, em 4 níveis, hierárquicos, de exposição do projeto.

### Nível 1

O primeiro nível consiste numa apresentação geral, tendo como objetivo a fácil análise do esquema por todos os não entendidos no tema.

### Nível 2

O segundo nível tem como função representar, com pormenor, a arquitetura do software. Vai ainda analisar todos os _containers_ (a análise de todas as escolhas a nível tecnológico que irá ser adotado para a formulação do _software_ e como comunicam entre elas).

### Nível 3

O terceiro nível mostra como é composto cada _container_, indicando o número de componentes, o que cada um desses componentes faz, as suas responsabilidades para o funcionamento do _container_ e alguns detalhes da tecnologia/implementação usada.

### Nível 4

O último nível consiste na explicação das porções de código mais importantes para o desenvolvimento de cada componente.
No entanto este nível é implementado apenas se a equipa achar pertinente implementar.

##### Notas finais

A nossa arquitetura C4 apenas irá conter 3 níveis, a não ser que com o desenvolvimento do software seja pertinente a criação do último nível.
