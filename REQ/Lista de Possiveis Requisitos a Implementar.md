**Lista de Possíveis Requisitos a Implementar**


- **4**	4.1	Os eventos na linha temporal devem estar organizados por commit instants.

- **8**	4.2	Deverá ser apresentada a árvore de ficheiros do repositório.

- **21**	4.4.1	O esforço total aplicado por categoria (REQ, PM, DEV, TST…) deve ser visualizável.

- **23**	4.4.2	Deve ser possível ver o perfil de cada membro.

- **26**	4.4.2	Deve ser possível ver a lista de commits de cada membro.

- **28**	4.5	A timeline de issues deve ser visualizável (abertos, ativos e total).~
- **33**	5	"Apenas administradores podem apagar info sobre um projecto (na dashboard, não no repositório gitlab associado). A dashboard apenas lê info do repositório, não o modifica."
