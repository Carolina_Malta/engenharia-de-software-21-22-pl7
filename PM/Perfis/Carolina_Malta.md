Carolina Sofia Gouveia Malta
2019210025

uc2019210025@student.uc.pt

Equipa de Qualidade

Licenciatura em Design e Multimédia 

Links Uteis 

- [Link para Board](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/boards?scope=all&author_username=Carolina_Malta)


- [Link para todos os issues](https://gitlab.com/dashboard/issues?scope=all&state=all&assignee_username=Carolina_Malta
)

ENV
- [Estudo GitLab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/3)

PM
- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/45)
- [Presença Reuniões](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/238)

REQ
- [User Story 8](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/109)

Atas
- [Ata 02/11/2021](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/149)

QA
- [Atividade geral](https://gitlab.com/users/Carolina_Malta/activity)
- [Testes para o projeto da PL5](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/328)

Papel no Projeto

Fui membro da equipa de Qualidade. 
Verifiquei issues quanto a forma e quanto ao processo.
Realizei tambem parte da lista de testes para executar no projeto da PL5.
