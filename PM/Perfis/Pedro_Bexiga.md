Pedro Duarte Ferreira de Matos Bexiga 2019200520

Equipa de Requisitos

uc2019200520@student.uc.pt

Licenciatura em Design e Multimédia

**Lista de Issues**

ENV

1. [Estudo Gitlab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/21)


PM

1. [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/39)
2. [Presença Reuniões](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/224)


REQ

1. [Atribuição de Requisitos](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/184)
2. [User Story 19](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/95)
3. [User Story 17](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/316)
4. [User Story 15](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/315)
5. [Lista de Requisitos de Prioridade Média](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/157)
6. [Lista de Requisitos a Não Implementar](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/223)
7. [Guardar Lista de Requisitos a Não Implementar No Repo](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/225)

**Papel no Projeto**

Fui membro da equipa de requisitos. Realizei tarefas destinadas à organização dos requisitos do projeto.

