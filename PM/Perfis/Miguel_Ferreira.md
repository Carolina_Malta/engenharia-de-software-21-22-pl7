# Perfil de Miguel Ferreira
- **Nome Completo:** Miguel Antunes Carvalho Portugal Ferreira
- **Número de Estudante:** 2019214567
- **Funções:** 
    - Backend Developer
- **E-mail:** uc2019214567@student.uc.pt
- **Curso:** Licenciatura em Engenharia Informática

<br>

# Lista de Issues

***Env***

- [Estudo Gitlab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/16)
- [Estudo Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/62)
- [Estudo Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/120)
- [Exploracao da API do Git](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/141)
- [Estudo pytest](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/201)

***PM***

- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/58)
- [Presenca Reunioes](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/212)
- [Atualizacao Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/312)

***REQ***

- [User Story 24](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/101)

***DEV***
- [Criacao do Projeto Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/158)
- [Criacao do Hello World](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/166)
- [Guia de Criacao e Configuracao de Base de Dados](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/260)
- [Implentacao: Timeline de Issues UST28](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/301)

<br>

# Papel no Projeto

Durante a realização do projeto fui responsável pelo desenvolvimento da parte de backend. Nas primeiras semanas estudei as ferramentas necessárias para o desenvolvimento do projeto, usando as seguintes para as implementações necessárias. Além disto, fiz a comunicação entre backend e frontend, resolvendo vários problemas em relação à execução do programa (ativação da venv, base de dades, etc).