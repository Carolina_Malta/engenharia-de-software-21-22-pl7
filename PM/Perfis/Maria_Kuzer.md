   # Perfil
   
   Nome: Maria Eduarda Simões Ferrão Marques Lourenço Kuzer

   Número: 2018252311
   
   equipa qualidade e requisitos
   
   Email: uc2018252311@student.uc.pt
   
   Curso: Licenciatura em Design e Multimedia
   
   Contribuicoes:
   
   ~QA
   - [Revisao de Merge Requests](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/2)
   - [Revisao Processo Issues](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/85)
   - [Revisao Atribuicao Requisitos](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/87)
   - [Revisao Issues](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/192)
   - [Revisao Issues](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/261)
   
   ~PM
   - [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/42)
   - [Ata Reuniao 05/10/2021](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/49)
   - [Ata Reniao 19/10/2021](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/126)
   - [ta Reuniao 16/11/2021](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/190)
   - [Presenca Reunioes](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/243)
   
   ~ENV
   - [Estudo Gitlab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/8)
   - [Estudo Python](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/142)
   
   ~PROC
   - [Processo Ata](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/48)
   - [Teste Design UST 38](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/264)
   - [Teste Design UST 3a](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/287)
   - [Teste BlackBox Login UST 3a](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/288)
   
   ~Defeito
   - [Defeito no UST3](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/193)
   
   ~DES
   - [Criacao Mapa de Navegacao e Layouts](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/242)
   
   ~DEV
   - [Desenho de Testes Frontend](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/262)
   - [Criacao e Implementacao do Menu de Navegacao](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/272
   
   ~REQ
   - [User Story 13](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/80)
   - [Teste Design Vista Temporal UST 3](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/169)
   - [User Story 14](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/276)
   - [Uer Story 16](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/277)
   
   ~ARCH
   - [Criacao Mapa](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/263)
   
   Fui membto da equipa de qualidade, tendo feito também realizado tarefas de outras categorias.
