# Perfil de Daniel Portovedo

- **Nome Completo:** Daniel Gonçalves Portovedo
- **Número de estudante:** 2019214315
- **Função:**
  - Backend Developer
- **E-mail:** uc2019214315@student.uc.pt
- **Curso:** Licenciatura de Engenharia Informática

<br>

# Lista de Issues

***Env***

- [Estudo GitLab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/19)
- [Estudo Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/66)
- [Exploracao da API do Git](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/146)
- [Estudo Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/198)
- [Estudo pytest](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/199)

***PM***
- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/30)
- [Presenca Reunioes](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/230)
- [Atualizacao Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/303)

***Dev***
- [Implementacao Django Login UST 38](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/162)
- [Resolver Conflito de Merge](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/244)
- [Correcao: Barra temporal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/302)

***REQ***
- [User Story 7](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/84)
- [Implementacao: Tempo desde a ultima atualizacao presente no ecran UST7](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/296)

<br>

# Papel no projeto

Nas primeiras semanas realizei estudos relativos às tecnologias usadas no projeto. Após o estudo, fui developer e fiz requisitos nas últimas três semanas.