
# Perfil de José Gonçalves
- **Nome Completo:** José Miguel Rocha Gonçalves
- **Número de estudante:** 2019223292
- **Funções:**
    - Project Manager
    - Backend Developer
- **E-mail:** uc2019223292@student.uc.pt
- **Curso:** Licenciatura em Engenharia Informática

---

## Contribuições
### ENV
- [Estudo Gitlab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/5)
- [Requisicao de Maquina Virtual](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/143)
- [Configuracao Inicial Pipeline CI](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/147)
- [Criacao Maquina Producao](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/175)
- [Configuracao GitLab Runner](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/176)
- [Correcao Pipeline CI/CD](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/267)
- [Criacao Ramo staging](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/268)
- ...

### PM:
- [Organização de Milestones](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/97)
- [Reunião com Prof. A. Damasceno](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/131)
- [Revisao merge requests Perfis](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/132)
- [Criacao Grafico de Esforco](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/182)
- [Links Acesso Rapido Issues e Boards](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/226)
- [Comunicação DeiWare Link Requisitos](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/299)
- [Comunicacao Cliente Requisito a Implementar](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/300)
- ...

### REQ:
- [User Story 11](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/79)

### ARCH
- [Estudo Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/96)

### PROD
- [Configuracao Inicial Deployment](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/204)
- [Configuracao Base Dados Maquina Virtual](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/269)
- [Teste Deployment Maquina Virtual](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/271)
- [Preparacao Chaves Secretas Producao](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/283)

## Papel Principal no Projeto

Desempenhei o papel de Gestor de Projeto. Desde novembro também foi responsável pela configuração da _pipeline_ de CI/CD. Simultaneamente fui responsável pela configuração do ambiente de _deployment_.