João Afonso Vieira de Sousa

2019224599

        Equipa de qualidade

uc2019224599@student.uc.pt



Licenciatura em Engenharia Informática, FCTUC


Contribuições:

Como membro da equipa de qualidade, assegurei que os issues realizados pelos membros da empresa serviam como uma fonte eficiente de informação, estando de acordos com templates, um dos quais elaborei. Também, em conjunto com o colega Aníbal Rodrigues, elaborei testes de aceitação de backend.
Além disso, realizei outras tarefas com propósito de melhorar o funcionamento do projeto em termos de logística.

**ENV**

- [Estudo GitLab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/18)
- [Estudo Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/68)

**QA**

- [Revisão de atas](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/124)
- [Revisão de issues 23/11](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/237)
- [Revisão de issues 1/12](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/266)
- [Revisão de issues 14/12](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/322)

**PM**

- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/51)
- [Ata Reunião 12/10/2021](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/74)
- [Presenças em Reuniões](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/211)

**DEV**

- [Desenho de Testes Backend](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/258)

**PROC**

- [Processo User Story](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/164)


