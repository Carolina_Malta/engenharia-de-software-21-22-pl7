Carolina Rebelo de Almeida
2019208200

Equipa Frontend

uc2019208200@student.uc.pt

Licenciatura em Design e Multimedia

# Lista de issues 
ENV 
- [Estudo GitLab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/23)
- [Estudo Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/119)

PM
- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/60)
- [Ata Reunião](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/167)
- [Ata Reunião](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/196)
- [Presença Reuniões](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/233)

DEV
- [Implementação HTML CSS Login UST 38](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/159)
- [Vista Temporal CSS HTML JS UST3a](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/189)
- [Implementação HTML CSS Login UST 38 (atualização)](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/279)

REQ 
- [Opções de Logotipos](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/71)
- [User Story 5](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/111)
- [User Story 25](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/102)

# Papel no Projeto 
Fui membro da equipa de frontend. Realizei tarefas destinadas ao desenvolvimento do design visual do projeto.
